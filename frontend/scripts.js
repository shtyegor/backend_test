// ---------------------
// Форма відправки даних
// ---------------------

$('form').submit(function(){
    let firstname = $(this).find('input[name="firstname"]').val();
    let lastname = $(this).find('input[name="lastname"]').val();
    let email = $(this).find('input[name="email"]').val();

    let data = `firstname=${firstname}&lastname=${lastname}&email=${email}`;

    $.ajax({
        type: 'POST',
        url: 'СТОРІНКА_КУДИ_БУДЕ_НАДІСЛАНИЙ_ЗАПИТ',
        data: data,
        success: function(data){
            if(data.status == 'success'){
                $('#form-error').hide();
                let html = `
                    <div class="user" data-id="${data.id}">
                        <div><span>${data.firstname}</span></div>
                        <div><span>${data.lastname}</span></div>
                        <div><span>${data.email}</span></div>
                        <div><button class="del">Видалити користувача</button></div>
                    </div>
                `;
                $('.users-table').append(html);
            }else if(data.status == 'error'){
                $('#form-error span').text(data.message);
                $('#form-error').show();
                setTimeout(function(){
                    $('#form-error').hide();
                }, 2000);
            }
        }
    });
    
    return false;
});


// ---------------------
// Видалення користувача
// ---------------------

$('.user .del').click(function(){
    let id = $(this).closest('.user').attr('data-id');

    let data = `id=${id}`;

    $.ajax({
        type: 'POST',
        url: 'СТОРІНКА_КУДИ_БУДЕ_НАДІСЛАНИЙ_ЗАПИТ',
        data: data,
        success: function(data){
            if(data.status == 'success'){
                $('#table-error').hide();
                $(`.user[data-id="${data.id}"]`).remove();
            }else if(data.status == 'error'){
                $('#table-error span').text(data.message);
                $('#table-error').show();
                setTimeout(function(){
                    $('#table-error').hide();
                }, 2000);
            }
        }
    });
    
    return false;
});